<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" />
    <title>Autos</title>
</head>

<body>
    <br />
    <?php
        include 'conexion.php';
        $conn = OpenCon();
        $sql = "SELECT autos.*, marcas.nombre as 'nombre_marca' FROM autos INNER JOIN marcas on marcas.id = autos.idMarca WHERE autos.id = '".$_GET['codigo']."'";
        foreach($conn->query($sql) as $row){
            $codigo = $row["id"];
            $nombre = $row["nombre"];
            $descripcion = $row["descripcion"];
            $tipoCombustible = $row["tipoCombustible"];
            $cantidadPuertas = $row["cantidadPuertas"];
            $precio = $row["precio"];
            $nombre_marca = $row["nombre_marca"];
        }   
    ?>

    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">NOMBRE</th>
                    <th scope="col">DESCRIPCION</th>
                    <th scope="col">COMBUSTIBLE</th>
                    <th scope="col">PUERTAS</th>
                    <th scope="col">PRECIO</th>
                    <th scope="col">MARCA</th>
                </tr>
            </thead>
            <tbody>
                    <tr>
                        <td><?php echo $codigo; ?></td>
                        <td><?php echo $nombre; ?></td>
                        <td><?php echo $descripcion; ?></td>
                        <td><?php echo $tipoCombustible; ?></td>
                        <td><?php echo $cantidadPuertas; ?></td>
                        <td><?php echo $precio; ?></td>
                        <td><?php echo $nombre_marca; ?></td>
                    </tr>
            </tbody>
        </table>
        <a class="btn btn-info" href="listar_autos.php">Regresar</a>
    </div>

    <?php
        Closecon($conn);
        
    ?>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>
</body>

</html>